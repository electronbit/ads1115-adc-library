## [1.0.2] - 2020-08-29
 
### Added
 
### Changed

Updated examples
   
### Fixed

## [1.0.1] - 2020-08-28
 
### Added

Added examples
 
### Changed

Removed some unused variables
   
### Fixed