# ADS1115
Driver for ADS1115 16-bit ADC from Texas Instruments

## Examples
Examples are available in `examples` directory.

## License
MIT License